package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Thilo on 11/5/2015.
 */
public class MathViewMul extends MathViewSum {

    MathViewMul(Context ctx, @NonNull String text)
    {
        super(ctx, text);
    }

    @Override
    protected void loadSignImage()
    {
        mSignImage = UIUtils.createTintedSvgDrawable(getResources(), R.raw.mul_sign, mColor);
        mSignImageFocused = UIUtils.createTintedSvgDrawable(getResources(), R.raw.mul_sign, mFocusColor);
    }

    @Override
    public String getText()
    {
        return "#mul(" + mUpperBounds.getText() + "," + mLowerBounds.getText() + "," + mFormulaView.getText() + ")";
    }
}
