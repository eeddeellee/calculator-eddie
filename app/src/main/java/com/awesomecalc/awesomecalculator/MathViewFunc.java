package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewFunc extends CompositeMathView {

    protected TextView mFunctionNameView;
    protected float BRACE_SIZE = 1/5f;
    protected float NAME_BRACE_PADDING = 1/20f;
    protected float COMMA_PADDING = 1/4f;
    protected float EMPTY_ARGUMENTS_PADDING = 1/7f;

    public MathViewFunc(Context ctx, @NonNull String text)
    {
        super(ctx, text);

        final MathView thisView = this;

        if(!mFunctionNameView.hasOnClickListeners())
            mFunctionNameView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    thisView.onClick(thisView);
                }
            });
    }

    @Override
    protected void parseText(String text)
    {
        if(mFunctionNameView == null) {
            mFunctionNameView = new TextView(getContext());

            mFunctionNameView.setTextColor(mColor);
            mFunctionNameView.setTypeface(mFont);
            mFunctionNameView.setTextSize(mTextSize);

            addView(mFunctionNameView, new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        }
        
        mFunctionNameView.setText(MathParser.functionNameFromToken(text));
        
        super.parseText(text);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(mFunctionNameView == null)
            throw new IllegalStateException("view not initialized properly");

        applyPaddingAndMargin();

        //measure all children
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));
        }

        int width = 0;

        //sum width of all Views
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
            width += UIUtils.getMarginLeft(child) + child.getMeasuredWidth() + UIUtils.getMarginRight(child);
        }

        int heightAboveCenter = mFunctionNameView.getMeasuredHeight()/2 + UIUtils.getMarginTop(mFunctionNameView);
        int heightBelowCenter = mFunctionNameView.getMeasuredHeight()/2 + UIUtils.getMarginBottom(mFunctionNameView);

        //get the maximum height of all children, start at 1 to ignore the functionNameView
        for (int i = 1; i < getChildCount(); i++) {
            final View child = getChildAt(i);

            heightAboveCenter = Math.max(heightAboveCenter, getAlignmentBaseline(child) + UIUtils.getMarginTop(child));
            heightBelowCenter = Math.max(heightBelowCenter, child.getMeasuredHeight() - getAlignmentBaseline(child) + UIUtils.getMarginBottom(child));
        }

        if(BuildFlags.VERTICAL_SYMMETRIC_BRACES)
            heightAboveCenter = heightBelowCenter = Math.max(heightAboveCenter, heightBelowCenter);

        mAlignmentBaseline = heightAboveCenter + getPaddingTop();

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + width + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        if(mFunctionNameView == null)
            throw new IllegalStateException("view not initialized properly");

        mFunctionNameView.layout(
                getPaddingLeft(),
                mAlignmentBaseline - mFunctionNameView.getMeasuredHeight()/2,
                getPaddingLeft() + mFunctionNameView.getMeasuredWidth(),
                mAlignmentBaseline + mFunctionNameView.getMeasuredHeight()/2);

        //layout all other children, start at 1 to ignore functionNameView
        for (int i = 1; i < getChildCount(); i++) {
            MathView child = (MathView) getChildAt(i);
            View childBefore = getChildAt(i-1);

            child.layout(
                    childBefore.getRight() + UIUtils.getMarginRight(childBefore) + UIUtils.getMarginLeft(child),
                    mAlignmentBaseline - getAlignmentBaseline(child),
                    childBefore.getRight() + UIUtils.getMarginRight(childBefore) + UIUtils.getMarginLeft(child) + child.getMeasuredWidth(),
                    mAlignmentBaseline + child.getMeasuredHeight() - getAlignmentBaseline(child));
        }
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);
        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        if(mFunctionNameView == null)
            throw new IllegalStateException("view not initialized properly");

        mFunctionNameView.setTextColor(mDrawingPaint.getColor());

        Rect oldClipBounds = canvas.getClipBounds();

        Rect leftClipBounds = new Rect(
                mFunctionNameView.getRight() + (int)(mTextSize * NAME_BRACE_PADDING),
                getPaddingTop(),
                mFunctionNameView.getRight() + (int)(mTextSize * NAME_BRACE_PADDING) + (int)(mTextSize * BRACE_SIZE),
                getHeight() - getPaddingBottom());

        Rect rightClipBounds = new Rect(
                getWidth() - getPaddingRight() - (int)(mTextSize * BRACE_SIZE),
                getPaddingTop(),
                getWidth() - getPaddingRight(),
                getHeight() - getPaddingBottom());

        canvas.clipRect(leftClipBounds, Region.Op.REPLACE);

        //draw left brace
        canvas.drawArc(
                new RectF(
                        leftClipBounds.left + mStrokeWidth,
                        -leftClipBounds.height() + leftClipBounds.top,
                        leftClipBounds.left + leftClipBounds.width() * 20,
                        leftClipBounds.height() * 2 + leftClipBounds.top),
                90, 180,
                false, mDrawingPaint);

        canvas.clipRect(rightClipBounds, Region.Op.REPLACE);

        //draw right brace
        canvas.drawArc(
                new RectF(
                        rightClipBounds.right - rightClipBounds.width() * 20,
                        -rightClipBounds.height() + rightClipBounds.top,
                        rightClipBounds.right - mStrokeWidth,
                        rightClipBounds.height() * 2 + rightClipBounds.top),
                -270, -180,
                false, mDrawingPaint);

        canvas.clipRect(oldClipBounds, Region.Op.REPLACE);

        mDrawingPaint.setStyle(Paint.Style.FILL);

        if (getChildCount() > 2)
            for (int i = 1; i < getChildCount()-1; i++)
                canvas.drawText(",", getChildAt(i).getRight(), mAlignmentBaseline + calculateTextHeight()/2,  mDrawingPaint);



    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        UIUtils.addPadding(this,
                (int) (mTextSize / 15f),
                (int) (mTextSize / 5f),
                (int) (mTextSize / 10f),
                (int) (mTextSize / 5f));

        //extra space for the braces
        if(getChildCount() > 1) {
            UIUtils.setMarginRight(mFunctionNameView, (int) (mTextSize * NAME_BRACE_PADDING));
            UIUtils.setMarginLeft(getChildAt(1), (int) (mTextSize * BRACE_SIZE));
            UIUtils.setMarginRight(getChildAt(getChildCount()-1), (int) (mTextSize * BRACE_SIZE));
        }
        else {
            UIUtils.setMarginRight(mFunctionNameView,
                    (int)(mTextSize * NAME_BRACE_PADDING
                            + mTextSize * BRACE_SIZE * 2
                            + mTextSize * EMPTY_ARGUMENTS_PADDING * getChildCount() - 2));
        }

        //add margin for the commas that separate multiple arguments
        if(getChildCount() > 2) {
            for (int i = 1; i < getChildCount()-1; i++) {
                UIUtils.setMarginRight(getChildAt(i), (int)(mTextSize * COMMA_PADDING));
            }
        }
    }

    @Override
    public String getText()
    {
        if(getChildCount() > 0) {
            String buf = "#" + ((TextView) getChildAt(0)).getText() + "(";

            for (int i = 1; i < getChildCount(); i++) {
                buf += ((MathView) getChildAt(i)).getText();

                //if not last argument
                if (i != getChildCount() - 1)
                    buf += ",";
            }

            return buf + ")";
        }
        else
            throw new IllegalStateException("view not initialized completely");

    }

    @Override
    public MathView findCaret()
    {
        MathView v;

        if(mHasCaret)
            return this;

        //start at index 1 because of the functionNameView
        for (int i = 1; i < getChildCount() ; i++) {
            v = ((MathView) getChildAt(i)).findCaret();

            if(v != null)
                return v;
        }

        return null;
    }

}
