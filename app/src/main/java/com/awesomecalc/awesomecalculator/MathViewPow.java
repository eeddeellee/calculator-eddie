package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewPow extends CompositeMathView {
    MathView mBaseChild;
    MathView mExpoChild;

    public MathViewPow(Context ctx, @NonNull String text)
    {
        super(ctx, text);

        if (getChildCount() != 2)
            throw new MathParser.ParseException("pow() can not have more or less than two arguments");
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        applyPaddingAndMargin();

        mBaseChild = (MathView) getChildAt(0);
        mExpoChild = (MathView) getChildAt(1);

        mExpoChild.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));

        mBaseChild.measure(
                getChildMeasureSpec(widthMeasureSpec, getPaddingLeft() + getPaddingRight(),
                        mBaseChild.getLayoutParams().width),
                getChildMeasureSpec(heightMeasureSpec, getPaddingTop() + getPaddingBottom(),
                        mBaseChild.getLayoutParams().height));

        mExpoChild.measure(
                getChildMeasureSpec(widthMeasureSpec, getPaddingLeft() + getPaddingRight(),
                        mExpoChild.getLayoutParams().width),
                getChildMeasureSpec(heightMeasureSpec, getPaddingTop() + getPaddingBottom(),
                        mExpoChild.getLayoutParams().height));

        int heightAboveCenter = Math.round(getAlignmentBaseline(mBaseChild) - mBaseChild.getMeasuredHeight() / 4f + getAlignmentBaseline(mExpoChild));

        int heightBelowCenter = Math.max(mBaseChild.getMeasuredHeight() - getAlignmentBaseline(mBaseChild), mExpoChild.getMeasuredHeight() - heightAboveCenter);

        mAlignmentBaseline = heightAboveCenter + getPaddingTop();

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + mBaseChild.getMeasuredWidth() + mExpoChild.getMeasuredWidth() + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom() , heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        mBaseChild.layout(
                getPaddingLeft(),
                mAlignmentBaseline - getAlignmentBaseline(mBaseChild),
                getPaddingLeft() + mBaseChild.getMeasuredWidth(),
                mAlignmentBaseline - getAlignmentBaseline(mBaseChild) + mBaseChild.getMeasuredHeight());

        mExpoChild.layout(
                getPaddingLeft() + mBaseChild.getMeasuredWidth(),
                getPaddingTop(),
                getPaddingLeft() + mBaseChild.getMeasuredWidth() + mExpoChild.getMeasuredWidth(),
                getPaddingTop() + mExpoChild.getMeasuredHeight());
    }

    @Override
    public String getText() {
        return "#pow(" + ((MathView) getChildAt(0)).getText() + "," + ((MathView) getChildAt(1)).getText() + ")";
    }
}
