package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewSum extends CompositeMathView {
    protected ImageView mSignView;
    protected Drawable mSignImage;
    protected Drawable mSignImageFocused;
    protected MathView mUpperBounds;
    protected MathView mLowerBounds;
    protected MathView mFormulaView;
    protected TextView mIndexView;
    protected static float SYMBOL_PADDING = 1/10f;
    protected static float ADD_PADDING = 1/15f;


    public MathViewSum(Context ctx, @NonNull String text)
    {
        super(ctx, text);

        mSignView = new ImageView(getContext());

        loadSignImage();

        mSignView.setImageDrawable(mSignImage);
        mSignView.setLayerType(View.LAYER_TYPE_SOFTWARE, null); //without this the svg-image will not show
        mSignView.setAdjustViewBounds(true);
        addView(mSignView, 1, new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        mIndexView = new TextView(getContext());
        mIndexView.setText("n=");
        mIndexView.setPadding(0,0,(int)(mTextSize * 1/20f), 0);
        addView(mIndexView, 2, new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    }

    protected void loadSignImage()
    {
        mSignImage = UIUtils.createTintedSvgDrawable(getResources(), R.raw.sum_sign, mColor);
        mSignImageFocused = UIUtils.createTintedSvgDrawable(getResources(), R.raw.sum_sign, mFocusColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(getChildCount() < 4)
            throw new IllegalStateException("view not initialized properly");

        mUpperBounds = (MathView)getChildAt(0);
        mLowerBounds = (MathView)getChildAt(3);
        mFormulaView = (MathView)getChildAt(4);

        applyPaddingAndMargin();

        mUpperBounds.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));
        mLowerBounds.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));
        mIndexView.setTextSize(TypedValue.COMPLEX_UNIT_PX, Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));

        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);

            if(child == mSignView)
                continue;

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));
        }

        mSignView.measure(
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(mFormulaView.getMeasuredHeight() + UIUtils.getMarginTop(mFormulaView) + UIUtils.getMarginBottom(mFormulaView), MeasureSpec.EXACTLY));

        int heightAboveCenter = Math.max(mUpperBounds.getMeasuredHeight() + (int)(mSignView.getMeasuredHeight()/2f) + UIUtils.getMarginTop(mSignView),
                getAlignmentBaseline(mFormulaView));

        int heightBelowCenter = Math.max((int)(mSignView.getMeasuredHeight() / 2f) + UIUtils.getMarginBottom(mSignView) +
                        Math.max(mLowerBounds.getMeasuredHeight(), mIndexView.getMeasuredHeight()),
                mFormulaView.getMeasuredHeight() - getAlignmentBaseline(mFormulaView));

        mAlignmentBaseline = heightAboveCenter + getPaddingTop();

        int combinedWidth = Math.max(UIUtils.getMarginLeft(mSignView) + mSignView.getMeasuredWidth() + UIUtils.getMarginRight(mSignView) + mFormulaView.getMeasuredWidth(),
                Math.max(mUpperBounds.getMeasuredWidth(), mLowerBounds.getMeasuredWidth() + mIndexView.getMeasuredWidth()));

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + combinedWidth + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        mUpperBounds.layout(
                getPaddingLeft(),
                getPaddingTop(),
                getPaddingLeft() + mUpperBounds.getMeasuredWidth(),
                getPaddingTop() + mUpperBounds.getMeasuredHeight());

        mSignView.layout(
                getPaddingLeft() + UIUtils.getMarginLeft(mSignView),
                mUpperBounds.getBottom() + UIUtils.getMarginTop(mSignView),
                getPaddingLeft() + UIUtils.getMarginLeft(mSignView) + mSignView.getMeasuredWidth(),
                mUpperBounds.getBottom() + UIUtils.getMarginTop(mSignView) + mSignView.getMeasuredHeight());

        int indexViewTopGap = (getAlignmentBaseline(mLowerBounds) > mIndexView.getMeasuredHeight()/2f)
                ? getAlignmentBaseline(mLowerBounds) - (int)(mIndexView.getMeasuredHeight()/2f) : 0;

        mIndexView.layout(
                getPaddingLeft(),
                mSignView.getBottom() + UIUtils.getMarginBottom(mSignView) + indexViewTopGap,
                getPaddingLeft() + mIndexView.getMeasuredWidth(),
                mSignView.getBottom() + UIUtils.getMarginBottom(mSignView) + indexViewTopGap + mIndexView.getMeasuredHeight());

        int lowerBoundsTopGap = (mIndexView.getMeasuredHeight()/2f > getAlignmentBaseline(mLowerBounds))
                ? (int)(mIndexView.getMeasuredHeight()/2f) - getAlignmentBaseline(mLowerBounds) : 0;

        mLowerBounds.layout(
                mIndexView.getRight(),
                mSignView.getBottom() + UIUtils.getMarginBottom(mSignView) + lowerBoundsTopGap,
                mIndexView.getRight() + mLowerBounds.getMeasuredWidth(),
                mSignView.getBottom() + UIUtils.getMarginBottom(mSignView) + lowerBoundsTopGap + mLowerBounds.getMeasuredHeight());

        mFormulaView.layout(
                mSignView.getRight() + UIUtils.getMarginRight(mSignView),
                mSignView.getTop() + (int)(mSignView.getHeight()/2f) - getAlignmentBaseline(mFormulaView),
                mSignView.getRight() + UIUtils.getMarginRight(mSignView) + mFormulaView.getMeasuredWidth(),
                mSignView.getTop() + (int)(mSignView.getHeight()/2f) - getAlignmentBaseline(mFormulaView) + mFormulaView.getMeasuredHeight());
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);

        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        mIndexView.setTextColor(mDrawingPaint.getColor());

        if(mHasCaret)
            mSignView.setImageDrawable(mSignImageFocused);
        else
            mSignView.setImageDrawable(mSignImage);
    }

    @Override
    public void setColor(int color)
    {
        super.setColor(color);
        loadSignImage();
    }

    @Override
    public void setFocusColor(int color)
    {
        super.setFocusColor(color);
        loadSignImage();
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();
        
        UIUtils.addPadding(this,
                (int)(mTextSize * ADD_PADDING),
                0,
                (int)(mTextSize * ADD_PADDING),
                0);

        UIUtils.setMargins(mSignView,
                (int)(mTextSize * SYMBOL_PADDING),
                (int)(mTextSize * SYMBOL_PADDING),
                (int)(mTextSize * SYMBOL_PADDING),
                (int)(mTextSize * SYMBOL_PADDING));

        mIndexView.setPadding((int)(mTextSize * 1/20f), 0, (int)(mTextSize * 1/20f), 0);
    }

    @Override
    public MathView findCaret()
    {
        MathView v = null;

        if(mHasCaret)
            return this;

        for (int i = 0; i < getChildCount() ; i++)
        {
            if(!(getChildAt(i) instanceof MathView))
                continue;

            v = ((MathView) getChildAt(i)).findCaret();

            if(v != null)
                return v;
        }

        return v;
    }

    @Override
    public String getText()
    {
        return "#sum(" + mUpperBounds.getText() + "," + mLowerBounds.getText() + "," + mFormulaView.getText() + ")";
    }
}
