package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.NonNull;
import android.view.View;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewRoot extends CompositeMathView {
    MathView mIndexChild;
    MathView mRadicandChild;

    public MathViewRoot(Context ctx, @NonNull String text)
    {
        super(ctx, text);

        if (getChildCount() != 2)
            throw new MathParser.ParseException("#root() can not have more or less than two arguments");


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        mIndexChild = (MathView) getChildAt(0);
        mRadicandChild = (MathView) getChildAt(1);

        applyPaddingAndMargin();

        mIndexChild.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));

        //measure all children
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));
        }

        int combinedWidth = mIndexChild.getMeasuredWidth()
                + UIUtils.getMarginLeft(mRadicandChild) + mRadicandChild.getMeasuredWidth() + UIUtils.getMarginRight(mRadicandChild);

        int heightAboveCenter = Math.max(
                getAlignmentBaseline(mIndexChild) + Math.round((mIndexChild.getMeasuredHeight() - getAlignmentBaseline(mIndexChild))/2f) ,
                getAlignmentBaseline(mRadicandChild) + UIUtils.getMarginTop(mRadicandChild));

        int heightBelowCenter = mRadicandChild.getMeasuredHeight() - getAlignmentBaseline(mRadicandChild) + UIUtils.getMarginBottom(mRadicandChild);

        mAlignmentBaseline = heightAboveCenter + getPaddingTop();

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + combinedWidth + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        mIndexChild.layout(
                getPaddingLeft(),
                Math.max(
                        Math.round(getMeasuredHeight()/2f - mIndexChild.getMeasuredHeight() + (mIndexChild.getMeasuredHeight() - getAlignmentBaseline(mIndexChild))/2f),
                        getPaddingTop()),
                getPaddingLeft() + mIndexChild.getMeasuredWidth(),
                Math.max(
                        Math.round(getMeasuredHeight()/2f + (mIndexChild.getMeasuredHeight() - getAlignmentBaseline(mIndexChild))/2f),
                        getPaddingTop() + mIndexChild.getMeasuredHeight()));

        int radicandChildTop = getAlignmentBaseline(this) - getAlignmentBaseline(mRadicandChild);

        mRadicandChild.layout(
                mIndexChild.getRight() + UIUtils.getMarginLeft(mRadicandChild),
                getPaddingTop() + radicandChildTop,
                mIndexChild.getRight() + UIUtils.getMarginLeft(mRadicandChild) + mRadicandChild.getMeasuredWidth(),
                getPaddingTop() + radicandChildTop + mRadicandChild.getMeasuredHeight());
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);

        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        if(mHasCaret)
            mDrawingPaint.setColor(mFocusColor);

        final View indexChild = getChildAt(0);
        final View radicandChild = getChildAt(1);
        Path rootSymbolPath = new Path();

        rootSymbolPath.moveTo(
                indexChild.getRight() - (radicandChild.getLeft() - indexChild.getRight()) / 1.2f,
                indexChild.getBottom() + (radicandChild.getBottom() - indexChild.getBottom()) / 4);

        //right
        rootSymbolPath.lineTo(
                indexChild.getRight() - (radicandChild.getLeft() - indexChild.getRight()) / 2,
                indexChild.getBottom() + mStrokeWidth);

        //down
        rootSymbolPath.lineTo(indexChild.getRight(), radicandChild.getBottom());

        //up
        rootSymbolPath.lineTo(
                radicandChild.getLeft() - mStrokeWidth * 2,
                radicandChild.getTop() - mStrokeWidth * 2);

        //right
        rootSymbolPath.lineTo(
                radicandChild.getRight() + mStrokeWidth * 2,
                radicandChild.getTop() - mStrokeWidth * 2);

        //down
        rootSymbolPath.rLineTo(0, mTextSize / 6);

        canvas.drawPath(rootSymbolPath, mDrawingPaint);
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        //padding between caret and root symbol
        UIUtils.addPadding(
                this,
                0,
                0,
                (int) (mTextSize * CARET_CORNER_RADIUS / 3),
                (int) (mTextSize * CARET_CORNER_RADIUS / 2.5f));

        //apply margin to make space for the radical/root symbol
        UIUtils.setMargins(
                mRadicandChild,
                (int) (mTextSize / 3f),
                (int) (mTextSize / 5f),
                (int) (mTextSize / 5f),
                (int) (mTextSize / 10f));
    }


    @Override
    public String getText()
    {
        return "#root(" + ((MathView) getChildAt(0)).getText() + "," + ((MathView) getChildAt(1)).getText() + ")";
    }
}
