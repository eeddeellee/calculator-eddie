package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.view.View;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewFraction extends CompositeMathView {
    private final float FRACTION_LINE_PADDING = 1/8f;
    private MathView mNumeratorChild;
    private MathView mDenominatorChild;

    public MathViewFraction(Context ctx, @NonNull String text)
    {
        super(ctx, text);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if(getChildCount() != 2)
            throw new RuntimeException("#fract() can not have more or less than 2 childs");

        mNumeratorChild = (MathView)getChildAt(0); //upper child
        mDenominatorChild = (MathView)getChildAt(1); //lower child;

        applyPaddingAndMargin();

        mNumeratorChild.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));
        mDenominatorChild.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));

        for (int i = 0; i < getChildCount(); i++)
        {
            final View child = getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));
        }

        mAlignmentBaseline = mNumeratorChild.getBottom() +  (int)(UIUtils.getMarginBottom(mNumeratorChild) / 2f);

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + Math.max(mNumeratorChild.getMeasuredWidth(), mDenominatorChild.getMeasuredWidth()) + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop()
                        + mNumeratorChild.getMeasuredHeight() + UIUtils.getMarginBottom(mNumeratorChild)
                        + mDenominatorChild.getMeasuredHeight() + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        mNumeratorChild.layout(
                Math.round(getWidth() / 2f - mNumeratorChild.getMeasuredWidth() / 2f),
                getPaddingTop(),
                Math.round(getWidth() / 2f + mNumeratorChild.getMeasuredWidth() / 2f),
                getPaddingTop() + mNumeratorChild.getMeasuredHeight());

        mDenominatorChild.layout(
                Math.round(getWidth() / 2f - mDenominatorChild.getMeasuredWidth() / 2f),
                mNumeratorChild.getBottom() + UIUtils.getMarginBottom(mNumeratorChild),
                Math.round(getWidth() / 2f + mDenominatorChild.getMeasuredWidth() / 2f),
                mNumeratorChild.getBottom() + UIUtils.getMarginBottom(mNumeratorChild) + mDenominatorChild.getMeasuredHeight());
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);

        mDrawingPaint.setFlags(mDrawingPaint.getFlags() & ~Paint.ANTI_ALIAS_FLAG);

        canvas.drawLine(
                getPaddingLeft(),
                Math.round(mNumeratorChild.getBottom() + (mDenominatorChild.getTop() - mNumeratorChild.getBottom()) / 2f),
                getWidth() - getPaddingRight(),
                Math.round(mNumeratorChild.getBottom() + (mDenominatorChild.getTop() - mNumeratorChild.getBottom()) / 2f),
                mDrawingPaint);
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        UIUtils.addPadding(this, (int) (mTextSize / 7f), 0, (int) (mTextSize / 7f), 0);

        //apply margin to make space for the line inbetween numerator and denominator
        UIUtils.setMarginBottom(mNumeratorChild, (int) (mTextSize * FRACTION_LINE_PADDING));
    }

    @Override
    public String getText()
    {
        return "#fract(" + mNumeratorChild.getText() + "," + mDenominatorChild.getText() + ")";
    }
}
