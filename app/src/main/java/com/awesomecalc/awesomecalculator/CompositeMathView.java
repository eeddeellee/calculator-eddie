package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Thilo on 16.07.2015.
 *
 * base class for a MathView that has other MathViews as children
 */
public class CompositeMathView extends MathView {

    public CompositeMathView(Context ctx, @NonNull String text)
    {
        super(ctx, text);
    }

    @Override
    public void onClick(View v)
    {
        MathView currentlySelectedView;

        if(mHasCaret) {
            mHasCaret = false;
            UIUtils.recursiveInvalidateChilds(this);
        }
        else {
            currentlySelectedView = getTopMostMathView().findCaret();

            if(currentlySelectedView != null) {
                currentlySelectedView.mHasCaret = false;
                UIUtils.recursiveInvalidateChilds(currentlySelectedView);
            }

            mHasCaret = true;
            UIUtils.recursiveInvalidateChilds(this);
        }
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        //padding for the caret
        setPadding(
                (int) mStrokeWidth,
                (int) mStrokeWidth,
                (int) mStrokeWidth,
                (int) mStrokeWidth);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev)
    {
        if(ev.getAction() == MotionEvent.ACTION_DOWN)
        {
            if(grandchildHasCaret())
                return false;

            if(!mHasCaret)
                return true;
        }

        return super.onInterceptTouchEvent(ev);
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);

        if(mHasCaret)
            drawCaret(canvas);

        mDrawingPaint.setTypeface(mFont);
        mDrawingPaint.setTextSize(mTextSize);
        mDrawingPaint.setStrokeWidth(mStrokeWidth);

        if(mHasCaret)
            mDrawingPaint.setColor(mFocusColor);
        else if(grandparentHasCaret()) {
            mDrawingPaint.setColor(mFocusColor);
            UIUtils.recursiveInvalidateChilds(this);
        }
        else
            mDrawingPaint.setColor(mColor);
    }

    protected void drawCaret(@NonNull Canvas canvas)
    {
        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mDrawingPaint.setColor(mFocusColor);
        mDrawingPaint.setStrokeWidth(mStrokeWidth);

        mBackgroundRect.left = 0 + mDrawingPaint.getStrokeWidth()/2;
        mBackgroundRect.top = 0 + mDrawingPaint.getStrokeWidth()/2;
        mBackgroundRect.right = getWidth() - mDrawingPaint.getStrokeWidth()/2;
        mBackgroundRect.bottom = getHeight() - mDrawingPaint.getStrokeWidth()/2;

        canvas.drawRoundRect(mBackgroundRect, mTextSize * CARET_CORNER_RADIUS, mTextSize * CARET_CORNER_RADIUS, mDrawingPaint);
    }
}
