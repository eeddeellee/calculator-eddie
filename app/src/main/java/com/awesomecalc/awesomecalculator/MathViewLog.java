package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewLog extends CompositeMathView {

    View mLogChild;
    MathView mBaseChild;
    MathView mNumerusChild;

    public MathViewLog(Context ctx, @NonNull String text)
    {
        super(ctx, text);
    }

    @Override
    protected void parseText(String text)
    {
        if(getChildAt(0) == null) {
            //add "log" child
            TextView v = new TextView(getContext());
            v.setText("log");
            v.setPadding((int) (mTextSize * TOKEN_PADDING_FACTOR), 0, 0, 0);
            v.setTextSize(mTextSize);
            v.setTextColor(mColor);
            v.setTypeface(mFont);

            final MathView thisView = this;

            v.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    thisView.onClick(thisView);
                }
            });

            if(BuildFlags.SHOW_MATHVIEW_VIEW_BOUNDS)
                v.setBackground(getResources().getDrawable(R.drawable.view_bounds));

            addView(v, new MarginLayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        }

        super.parseText(text);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        if (getChildCount() != 3)
            throw new MathParser.ParseException("log() can not have more or less than two arguments");

        mLogChild = getChildAt(0);
        mBaseChild = (MathView) getChildAt(1);
        mNumerusChild = (MathView) getChildAt(2);

        applyPaddingAndMargin();

        mBaseChild.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));

        int heightAboveCenter;
        int heightBelowCenter;

        //measure all children
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));
        }

        int combinedWidth = mLogChild.getMeasuredWidth() + mBaseChild.getMeasuredWidth() + UIUtils.getMarginRight(mBaseChild)
                + UIUtils.getMarginLeft(mNumerusChild) + mNumerusChild.getMeasuredWidth() + UIUtils.getMarginRight(mNumerusChild);

        if(BuildFlags.VERTICAL_SYMMETRIC_BRACES) {
            heightAboveCenter = Math.max(getAlignmentBaseline(mNumerusChild), mNumerusChild.getMeasuredHeight() - getAlignmentBaseline(mNumerusChild));

            heightBelowCenter = Math.max(mLogChild.getMeasuredHeight()/2 + mBaseChild.getMeasuredHeight()/3, heightAboveCenter);
        }
        else {
            heightAboveCenter = getAlignmentBaseline(mNumerusChild);

            heightBelowCenter = Math.max(
                    mLogChild.getMeasuredHeight()/2 + mBaseChild.getMeasuredHeight()/3,
                    mNumerusChild.getMeasuredHeight() - getAlignmentBaseline(mNumerusChild));
        }

        int baseBottom = getPaddingTop() + heightAboveCenter + mLogChild.getMeasuredHeight()/2 + mBaseChild.getMeasuredHeight()/3;

        //apply extra padding at the bottom if the braces are bigger than the baseChild
        if(baseBottom < getPaddingTop() + heightAboveCenter + heightBelowCenter)
            heightBelowCenter += (int)(mTextSize / 5f);

        mAlignmentBaseline = getPaddingTop() + heightAboveCenter;

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + combinedWidth + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        mNumerusChild.layout(
                getPaddingLeft() + mLogChild.getMeasuredWidth() + mBaseChild.getMeasuredWidth() + UIUtils.getMarginRight(mBaseChild) + UIUtils.getMarginLeft(mNumerusChild),
                getAlignmentBaseline(this) - getAlignmentBaseline(mNumerusChild),
                getPaddingLeft() + mLogChild.getMeasuredWidth() + mBaseChild.getMeasuredWidth() + mNumerusChild.getMeasuredWidth() +  UIUtils.getMarginLeft(mNumerusChild),
                getAlignmentBaseline(this) - getAlignmentBaseline(mNumerusChild) + mNumerusChild.getMeasuredHeight());

        mLogChild.layout(
                getPaddingLeft(),
                getAlignmentBaseline(this) - mLogChild.getMeasuredHeight() / 2,
                getPaddingLeft() + mLogChild.getMeasuredWidth(),
                getAlignmentBaseline(this) + mLogChild.getMeasuredHeight() / 2);

        mBaseChild.layout(
                mLogChild.getRight(),
                mLogChild.getBottom() - mBaseChild.getMeasuredHeight() * 2/3,
                mLogChild.getRight() + mBaseChild.getMeasuredWidth(),
                mLogChild.getBottom() + mBaseChild.getMeasuredHeight() / 3);
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);
        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        
        ((TextView) mLogChild).setTextColor(mDrawingPaint.getColor());

        Rect oldClipBounds = canvas.getClipBounds();

        Rect leftClipBounds;
        Rect rightClipBounds;

        if(BuildFlags.VERTICAL_SYMMETRIC_BRACES) {
            int braceHeight = Math.max(getAlignmentBaseline(mNumerusChild), mNumerusChild.getHeight() - getAlignmentBaseline(mNumerusChild))*2;

            leftClipBounds = new Rect(
                    mBaseChild.getRight(),
                    getAlignmentBaseline(this) - braceHeight / 2,
                    mNumerusChild.getLeft(),
                    getAlignmentBaseline(this) + braceHeight / 2);
        }
        else {
            leftClipBounds = new Rect(
                    mBaseChild.getRight(),
                    getAlignmentBaseline(this) - getAlignmentBaseline(mNumerusChild),
                    mNumerusChild.getLeft(),
                    getAlignmentBaseline(this) + mNumerusChild.getHeight() - getAlignmentBaseline(mNumerusChild));
        }

        rightClipBounds = new Rect(
                mNumerusChild.getRight(),
                leftClipBounds.top,
                getWidth() - getPaddingRight(),
                leftClipBounds.bottom);

        canvas.clipRect(leftClipBounds, Region.Op.REPLACE);

        //draw left brace
        canvas.drawArc(
                new RectF(
                        mBaseChild.getRight() + mStrokeWidth,
                        -leftClipBounds.height() + leftClipBounds.top,
                        mBaseChild.getRight() + leftClipBounds.width() * 20,
                        leftClipBounds.height() * 2 + leftClipBounds.top),
                90, 180,
                false, mDrawingPaint);

        canvas.clipRect(rightClipBounds, Region.Op.REPLACE);

        //draw right brace
        canvas.drawArc(
                new RectF(
                        rightClipBounds.right - rightClipBounds.width()*20,
                        -rightClipBounds.height() + rightClipBounds.top,
                        rightClipBounds.right - mStrokeWidth,
                        rightClipBounds.height() * 2 + rightClipBounds.top),
                -270, -180,
                false, mDrawingPaint);

        canvas.clipRect(oldClipBounds, Region.Op.REPLACE);
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        UIUtils.addPadding(this,
                (int) (mTextSize / 15f),
                (int) (mTextSize / 5f),
                (int) (mTextSize / 10f),
                0);

        //extra space for the braces
        UIUtils.setMarginLeft(mNumerusChild, (int)(mTextSize / 5f));
        UIUtils.setMarginRight(mNumerusChild, (int) (mTextSize / 5f));
        UIUtils.setMarginRight(mBaseChild, (int) (mTextSize / 20f));
    }

    @Override
    public String getText()
    {
        return "#log(" + ((MathView) getChildAt(0)).getText() + "," + ((MathView) getChildAt(1)).getText() + ")";
    }

    @Override
    public MathView findCaret()
    {
        MathView v;

        if(mHasCaret)
            return this;

        //start at index 1 because of the "log" child
        for (int i = 1; i < getChildCount() ; i++)
        {
            v = ((MathView) getChildAt(i)).findCaret();

            if(v != null)
                return v;
        }

        return null;
    }
}
