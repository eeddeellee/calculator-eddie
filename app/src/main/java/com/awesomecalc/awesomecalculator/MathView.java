package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathView extends ViewGroup implements ViewGroup.OnClickListener {
    private TypedArray mTypedAttributes;
    protected float mTextSize;
    protected float mMinTextSize; //0 if unlimited
    protected float mStrokeWidth = 2.5f;
    protected int mColor;
    protected int mFocusColor;
    protected int mFocusTextColor;
    protected Typeface mFont;
    protected boolean mReadOnly = true;
    protected boolean mHasCaret = false;
    protected Paint mDrawingPaint;
    protected int mAlignmentBaseline = 0;
    protected RectF mBackgroundRect;

    //every size is a multiple of mTextSize
    protected final float SUPERSCRIPT_TEXTSCALE_FACTOR = 7/10f; //factor by which superscript text like x^y gets scaled
    protected final float TOKEN_PADDING_FACTOR = 2/10f; //lr padding for every token, so background is wide enough
    protected final float TOKEN_MARGIN_FACTOR = 0; //lr spacing between the tokens
    protected final float CARET_CORNER_RADIUS = 1/3.5f;

    public MathView(Context ctx, @NonNull String text)
    {
        super(ctx);
        init(text);
    }

    public MathView(Context ctx, AttributeSet attrs)
    {
        super(ctx, attrs);
        mTypedAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.MathView, 0, 0);
        init(null);
    }

    public MathView(Context ctx, AttributeSet attrs, int defStyleAttr)
    {
        super(ctx, attrs, defStyleAttr);
        mTypedAttributes = getContext().obtainStyledAttributes(attrs, R.styleable.MathView, 0, 0);
        init(null);
    }

    private void init(String text)
    {
        mDrawingPaint = new Paint();
        mDrawingPaint.setTextAlign(Paint.Align.LEFT);
        mBackgroundRect = new RectF();

        setClickable(true);
        setOnClickListener(this);

        ColorStateList colorList = null;
        ColorStateList focusColorList = null;
        ColorStateList focusTextColorList = null;

        if(mTypedAttributes != null) {
            if(text == null)
                text = mTypedAttributes.getString(R.styleable.MathView_text);

            mTextSize = mTypedAttributes.getDimensionPixelSize(R.styleable.MathView_textSize, (int) new TextView(getContext()).getTextSize());
            mMinTextSize = mTypedAttributes.getDimensionPixelSize(R.styleable.MathView_minTextSize, 0);
            mStrokeWidth = mTypedAttributes.getDimension(R.styleable.MathView_strokeWidth, mStrokeWidth);
            colorList = mTypedAttributes.getColorStateList(R.styleable.MathView_normalColor);
            focusColorList = mTypedAttributes.getColorStateList(R.styleable.MathView_focusColor);
            focusTextColorList = mTypedAttributes.getColorStateList(R.styleable.MathView_focusTextColor);
            mReadOnly = mTypedAttributes.getBoolean(R.styleable.MathView_readOnly, true);
        }

        if(text == null)
            text = "";

        if(colorList == null)
            mColor = getResources().getColor(R.color.black);
        else if(colorList.isStateful())
            throw new IllegalArgumentException("color can not be stateful");
        else
            mColor = colorList.getDefaultColor();


        if(focusColorList == null)
            mFocusColor = getResources().getColor(R.color.light_green_A700);
        else if(focusColorList.isStateful())
            throw new IllegalArgumentException("focus color can not be stateful");
        else
            mFocusColor = focusColorList.getDefaultColor();


        if(focusTextColorList == null)
            mFocusTextColor = getResources().getColor(R.color.white);
        else if(focusTextColorList.isStateful())
            throw new IllegalArgumentException("focus text color can not be stateful");
        else
            mFocusTextColor = focusTextColorList.getDefaultColor();


        if(mFont == null)
            mFont = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Light.ttf");

        parseText(text);
    }

    @Override
    public void onClick(View v)
    {
        MathView currentlySelectedView;

        currentlySelectedView = getTopMostMathView().findCaret();

        if(currentlySelectedView != null) {
            currentlySelectedView.mHasCaret = false;
            UIUtils.recursiveInvalidateChilds(currentlySelectedView);
        }
    }

    protected void parseText(String text)
    {
        if(text.isEmpty()) {
            if(getChildCount() > 0)
                removeAllViews();

            return;
        }

        //split the expression into it's tokens
        ArrayList<String> tokenList = MathParser.tokenizeForView(text);

        //create for each token the corresponding view
        for(int i=0; i < tokenList.size(); i++) {
            MathView newView = null;
            MathView viewAtCurrentPos = (MathView)getChildAt(i + getNonMathViewChildCount()); //ignore children who aren't a MathView because they don't have a token
            String token = tokenList.get(i);

            //if there are already views, skip the views whose text hasn't changed
            if(viewAtCurrentPos != null && viewAtCurrentPos.getText().equals(token))
                continue;

            if(MathParser.hasSubTokens(token)) {
                if(MathParser.hasSubstringAt(token, "#pow(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewPow)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewPow(getContext(), token);
                }
                else if(MathParser.hasSubstringAt(token, "#integral(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewIntegral)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewIntegral(getContext(), token);
                }
                else if(MathParser.hasSubstringAt(token, "#sum(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewSum)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewSum(getContext(), token);
                }
                else if(MathParser.hasSubstringAt(token, "#mul(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewMul)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewMul(getContext(), token);
                }
                else if(MathParser.hasSubstringAt(token, "#log(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewLog)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewLog(getContext(), token);
                }
                else if(MathParser.hasSubstringAt(token, "#fract(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewFraction)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewFraction(getContext(), token);
                }
                else if(MathParser.hasSubstringAt(token, "#binom(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewBinomial)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewBinomial(getContext(), token);
                }
                else if(MathParser.hasSubstringAt(token, "#root(", 0)) {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewRoot)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewRoot(getContext(), token);
                }
                else if(token.charAt(0) == '#') {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewFunc)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewFunc(getContext(), token);
                }
                else {
                    throw new MathParser.ParseException("unknown top level token");
                }
            }
            else { //doesn't have sub tokens
                if(MathParser.isTokenChain(token)) {
                    if(viewAtCurrentPos != null)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathView(getContext(), token);
                }
                else if (token.charAt(0) == '(' || token.charAt(0) == ')') {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewBrace)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewBrace(getContext(), token);
                }
                else {
                    if(viewAtCurrentPos != null && viewAtCurrentPos instanceof MathViewText)
                        viewAtCurrentPos.setText(token);
                    else
                        newView = new MathViewText(getContext(), token);
                }
            }

            if(newView != null) {
                newView.setReadOnly(mReadOnly);
                newView.setColor(mColor);
                newView.setFocusColor(mFocusColor);
                newView.setTypeface(mFont);
                newView.setTextSize(mTextSize);
                newView.setStrokeWidth(mStrokeWidth);
                newView.setMinTextSize(mMinTextSize);

                if(BuildFlags.SHOW_MATHVIEW_VIEW_BOUNDS)
                    newView.setBackground(getResources().getDrawable(R.drawable.view_bounds));

                addView(newView, new MarginLayoutParams(
                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
            }
        }
    }

    protected static int getAlignmentBaseline(@NonNull View v)
    {
        if(v instanceof  MathView)
            return ((MathView) v).mAlignmentBaseline;
        else
            return v.getMeasuredHeight() / 2;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int combinedWidth = 0;

        //these combined are the height of this view
        int heightAboveCenter = 0; //maximum height of child views above the vertical center of this viewgroup
        int heightBelowCenter = 0; //maximum height of child views below the vertical center of this viewgroup

        //measure all childs except for braces ( ). They have to be measured later because their height depends on the other views
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);

            if (child instanceof MathViewBrace)
                continue;

            MarginLayoutParams lParams = (MarginLayoutParams) child.getLayoutParams();

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));

            combinedWidth += lParams.leftMargin + child.getMeasuredWidth() + lParams.rightMargin;

            heightAboveCenter = Math.max(
                    heightAboveCenter,
                    lParams.topMargin + getAlignmentBaseline(child));

            heightBelowCenter = Math.max(
                    heightBelowCenter,
                    lParams.bottomMargin + child.getMeasuredHeight() - getAlignmentBaseline(child));
        }

        //now measure all left braces in reverse order
        for (int i = getChildCount()-1; i >= 0; i--) {

            //if not left brace
            if ( !(getChildAt(i) instanceof MathViewBrace && ((MathViewBrace) getChildAt(i)).getOrientation() == MathViewBrace.Orientation.LEFT) )
                continue;

            final MathView child = (MathView)getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));

            heightAboveCenter = Math.max(
                    heightAboveCenter,
                    getAlignmentBaseline(child));

            heightBelowCenter = Math.max(
                    heightBelowCenter,
                    child.getMeasuredHeight() - getAlignmentBaseline(child));

            combinedWidth += UIUtils.getMarginLeft(child) + child.getMeasuredWidth() + UIUtils.getMarginRight(child);
        }

        //now measure all right braces in order after the left braces, because their height depends on the height of the corresponding left brace
        for (int i = 0; i < getChildCount(); i++) {

            //if not right brace
            if (  !(getChildAt(i) instanceof MathViewBrace && ((MathViewBrace) getChildAt(i)).getOrientation() == MathViewBrace.Orientation.RIGHT) )
                continue;

            final MathView child = (MathView)getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));

            heightAboveCenter = Math.max(
                    heightAboveCenter,
                    getAlignmentBaseline(child));

            heightBelowCenter = Math.max(
                    heightBelowCenter,
                    child.getMeasuredHeight() - getAlignmentBaseline(child));

            combinedWidth += UIUtils.getMarginLeft(child) + child.getMeasuredWidth() + UIUtils.getMarginRight(child);
        }

        mAlignmentBaseline = heightAboveCenter + getPaddingTop();

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + combinedWidth + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        int maxHeightAboveCenter = 0;

        for (int i = 0; i < getChildCount(); i++) {
            maxHeightAboveCenter = Math.max(maxHeightAboveCenter, getAlignmentBaseline(getChildAt(i)));
        }

        for (int i = 0; i < getChildCount(); i++)
        {
            View child = getChildAt(i);

            int childLeft = (i == 0) ? getPaddingLeft() : getChildAt(i-1).getRight();

            childLeft += UIUtils.getMarginLeft(child)
                    + ( (i >= 1) ? UIUtils.getMarginRight(getChildAt(i-1)) : 0 );

            int childRight = childLeft + child.getMeasuredWidth();

            int childTop = maxHeightAboveCenter - getAlignmentBaseline(child) + getPaddingTop();

            int childBottom = childTop + child.getMeasuredHeight();

            child.layout(childLeft, childTop, childRight, childBottom);
        }
    }

    public void setText(@NonNull String expression)
    {
        parseText(expression);
    }

    public String getText()
    {
        String text = "";

        for (int i = 0; i < getChildCount(); i++) {
            text += ((MathView) getChildAt(i)).getText();
        }

        return text;
    }

    /**
     ** maximum size for the text to scale to
     * @param size in scaled pixels, see docs for TextView.setTextSize, -1.0f for unlimited
     */
    public void setTextSize(float size)
    {
        if(size == mTextSize)
            return;

        mTextSize = size;

        for(int i = 0; i < getChildCount(); i++)
        {
            View c = getChildAt(i);

            if(c instanceof MathView)
                ((MathView) c).setTextSize(size);
            else if(c instanceof TextView)
                ((TextView) c).setTextSize(TypedValue.COMPLEX_UNIT_PX, size);
        }

        requestLayout();
        invalidate();
    }

    /**
     ** maximum size for the text to scale to
     * @param unit TypedValue enum, see docs for TextView.setTextSize
     * @param size text size in given unit, -1.0f for unlimited
     */
    public void setTextSize(int unit, float size)
    {
        setTextSize(TypedValue.applyDimension(unit, size, getResources().getDisplayMetrics()));
    }

    /**
     * @return -1.0f if unlimited
     */
    public float getTextSize()
    {
        return mTextSize;
    }

    /**
     ** maximum size for the text to scale to
     * @param size in scaled pixels, see docs for TextView.setTextSize, -1.0f for unlimited
     */
    public void setMinTextSize(float size)
    {
        if(size == mMinTextSize)
            return;

        mMinTextSize = size;

        for(int i = 0; i < getChildCount(); i++)
        {
            View c = getChildAt(i);

            if(c instanceof MathView)
                ((MathView) c).setMinTextSize(size);
        }
    }

    /**
     ** maximum size for the text to scale to
     * @param unit TypedValue enum, see docs for TextView.setTextSize
     * @param size text size in given unit, -1.0f for unlimited
     */
    public void setMinTextSize(int unit, float size)
    {
        setMinTextSize(TypedValue.applyDimension(unit, size, getResources().getDisplayMetrics()));
        requestLayout();
    }

    /**
     * @return -1.0f if unlimited
     */
    public float getMinTextSize()
    {
        return mMinTextSize;
    }

    public void setTypeface(@NonNull Typeface font)
    {
        if(font == mFont)
            return;

        mFont = font;

        for(int i = 0; i < getChildCount(); i++) {
            View v = getChildAt(i);

            if(v instanceof TextView) {
                ((TextView) v).setTypeface(font);
            }
            else if(v instanceof MathView) {
                ((MathView) v).setTypeface(font);
            }
        }

        requestLayout();
        invalidate();
    }

    public Typeface getTypeface()
    {
        return mFont;
    }

    public void setReadOnly(boolean readOnly)
    {
        mReadOnly = readOnly;
    }

    public boolean getReadOnly()
    {
        return mReadOnly;
    }

    /**
     * @param color stateless text color
     */
    public void setColor(int color)
    {
        if(color == mColor)
            return;

        mColor = color;

        for(int i = 0; i < getChildCount(); i++)
        {
            View child = getChildAt(i);

            if(child instanceof TextView)
                ((TextView) child).setTextColor(color);

            if(child instanceof MathView)
                ((MathView) child).setColor(color);
        }

        invalidate();
    }

    public int getColor()
    {
        return mColor;
    }

    public void setFocusColor(int color)
    {
        if(color == mFocusColor)
            return;

        mFocusColor = color;

        for(int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);

            if(child instanceof MathView)
                ((MathView) child).setFocusColor(color);
        }

        invalidate();
    }

    public int getFocusColor()
    {
        return mFocusColor;
    }

    public void setFocusTextColor(int color)
    {
        if(color == mFocusTextColor)
            return;

        mFocusTextColor = color;

        for(int i = 0; i < getChildCount(); i++)
        {
            View child = getChildAt(i);

            if(child instanceof MathView)
                ((MathView) child).setFocusTextColor(color);
        }

        invalidate();
    }

    public int getFocusTextColor()
    {
        return mFocusTextColor;
    }

    public void setStrokeWidth(float width)
    {
        if(width != mStrokeWidth) {
            mStrokeWidth = width;

            for(int i = 0; i < getChildCount(); i++) {
                View child = getChildAt(i);

                if(child instanceof MathView)
                    ((MathView) child).setStrokeWidth(width);
            }

            requestLayout();
            invalidate();
        }
    }

    public float getStrokeWidth()
    {
        return mStrokeWidth;
    }

    public MathView findCaret()
    {
        if(mHasCaret)
            return this;

        MathView v;

        for (int i = 0; i < getChildCount() ; i++) {
             v = ((MathView) getChildAt(i)).findCaret();

            if(v != null)
                return v;
        }

        return null;
    }

    protected boolean siblingHasCaret()
    {
        for (int i = 0; i < ((ViewGroup) getParent()).getChildCount(); i++)
        {
            MathView v = (MathView) ((ViewGroup) getParent()).getChildAt(i);

            if (v.mHasCaret)
                return true;
        }

        return false;
    }

    protected boolean grandparentHasCaret()
    {
        if(!(getParent() instanceof MathView))
            return false;

        MathView grandParent = (MathView) getParent();

        MathView topMostGrandparent = getTopMostMathView();

        while(grandParent != topMostGrandparent)
        {
            if(grandParent.mHasCaret)
                return true;

            grandParent = (MathView) grandParent.getParent();
        }

        return false;
    }
    
    protected boolean childHasCaret()
    {
        for (int i = 0; i < getChildCount(); i++) {
            if(getChildAt(i) instanceof MathView)
                if(((MathView) getChildAt(i)).mHasCaret)
                    return true;
        }

        return false;
    }

    protected boolean grandchildHasCaret()
    {
        for (int i = 0; i < getChildCount(); i++) {
            if(getChildAt(i) instanceof MathView)
            {
                if(((MathView) getChildAt(i)).mHasCaret)
                    return true;
                else if(((MathView) getChildAt(i)).grandchildHasCaret())
                    return true;
            }
        }

        return false;
    }

    public MathView getTopMostMathView()
    {
        return (MathView) UIUtils.getGrandparent(this, new UIUtils.Condition<View>() {
            @Override
            public boolean test(View testable)
            {
                return !(testable.getParent() instanceof MathView);
            }
        });
    }

    protected int calculateTextHeight()
    {
        Paint p = new Paint();
        p.setTextSize(mTextSize);
        p.setTypeface(mFont);
        p.setStyle(Paint.Style.FILL);
        Rect textBounds = new Rect();

        p.getTextBounds("3", 0, 1, textBounds);

        return textBounds.height();
    }

    protected int getNonMathViewChildCount()
    {
        int count = 0;

        for (int i = 0; i < getChildCount(); i++) {
            if(!(getChildAt(i) instanceof MathView))
                count++;
        }

        return count;
    }

    protected void applyPaddingAndMargin() {

    }
}
