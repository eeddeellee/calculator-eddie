package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.view.View;

/**
 * Created by Thilo on 31.07.2015.
 *
 * base class for a MathView that doesn't have other MathViews as children
 */
public class NonCompositeMathView extends MathView {

    public NonCompositeMathView(Context ctx, @NonNull String text)
    {
        super(ctx, text);
    }

    @Override
    public void onClick(View v)
    {
        MathView currentlySelectedView;

        if(mHasCaret) {
            mHasCaret = false;
            UIUtils.recursiveInvalidateChilds(this);
        }
        else {
            currentlySelectedView = getTopMostMathView().findCaret();

            if(currentlySelectedView != null) {
                currentlySelectedView.mHasCaret = false;
                UIUtils.recursiveInvalidateChilds(currentlySelectedView);
            }

            mHasCaret = true;
            UIUtils.recursiveInvalidateChilds(this);
        }
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);

        if(mHasCaret)
            drawCaret(canvas);

        mDrawingPaint.setTypeface(mFont);
        mDrawingPaint.setTextSize(mTextSize);
        mDrawingPaint.setStrokeWidth(mStrokeWidth);

        if(mHasCaret)
            mDrawingPaint.setColor(mFocusTextColor);
        else if(grandparentHasCaret()) {
            mDrawingPaint.setColor(mFocusColor);
            UIUtils.recursiveInvalidateChilds(this);
        }
        else
            mDrawingPaint.setColor(mColor);
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        setPadding(
                (int)(mTextSize * TOKEN_PADDING_FACTOR),
                (int)(mTextSize * TOKEN_PADDING_FACTOR),
                (int)(mTextSize * TOKEN_PADDING_FACTOR),
                (int)(mTextSize * TOKEN_PADDING_FACTOR));
    }

    protected void drawCaret(@NonNull Canvas canvas)
    {
        mDrawingPaint.setStyle(Paint.Style.FILL);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mDrawingPaint.setColor(mFocusColor);

        mBackgroundRect.left = 0;
        mBackgroundRect.top = 0;
        mBackgroundRect.right = getWidth();
        mBackgroundRect.bottom = getHeight();

        canvas.drawRoundRect(mBackgroundRect, mTextSize * CARET_CORNER_RADIUS, mTextSize * CARET_CORNER_RADIUS, mDrawingPaint);
    }

}
