package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewBrace extends NonCompositeMathView {

    private final float BRACE_WIDTH = 1/3f;
    private Orientation mOrientation;

    public enum Orientation { LEFT, RIGHT }

    public MathViewBrace(Context ctx, @NonNull String text)
    {
        super(ctx, text);
    }

    @Override
    protected void parseText(String text)
    {
        if(text.charAt(0) == '(')
            mOrientation = Orientation.LEFT;
        else
            mOrientation = Orientation.RIGHT;
    }

    protected int calculateLeftBraceHeight()
    {
        int heightAboveCenter = 0;
        int heightBelowCenter = 0;
        int nestedBracesDepth = 0;

        if(((MathView) getParent()).getChildCount()-1 > ((MathView) getParent()).indexOfChild(this)) {
            for (int i = ((MathView) getParent()).indexOfChild(this) + 1; i < ((MathView) getParent()).getChildCount(); i++) {
                final View sibling = ((MathView) getParent()).getChildAt(i);

                if (sibling instanceof MathViewBrace) {
                    if (((MathViewBrace) sibling).mOrientation == Orientation.LEFT)
                        nestedBracesDepth++;

                    if (((MathViewBrace) sibling).mOrientation == Orientation.RIGHT && nestedBracesDepth > 0)
                        nestedBracesDepth--;
                    else if (((MathViewBrace) sibling).mOrientation == Orientation.RIGHT)
                        break;
                }

                heightAboveCenter = Math.max(heightAboveCenter, getAlignmentBaseline(sibling));
                heightBelowCenter = Math.max(heightBelowCenter, sibling.getMeasuredHeight() - getAlignmentBaseline(sibling));
            }
        }
        else {
            heightAboveCenter = heightBelowCenter = calculateTextHeight() / 2 + (int)(mTextSize * TOKEN_PADDING_FACTOR);
        }

        int combinedHeight;

        if(BuildFlags.VERTICAL_SYMMETRIC_BRACES) {
            combinedHeight = Math.max(heightAboveCenter, heightBelowCenter) * 2;
            mAlignmentBaseline = getPaddingTop() + Math.max(heightAboveCenter, heightBelowCenter);
        }
        else {
            combinedHeight = heightAboveCenter + heightBelowCenter;
            mAlignmentBaseline = getPaddingTop() + heightAboveCenter;
        }

        return combinedHeight;
    }

    protected int calculateRightBraceHeight()
    {
        View matchingBrace = null;
        int nestedBracesDepth = 0;
        int height;

        for(int i = ((ViewGroup) getParent()).indexOfChild(this)-1; i >= 0; i--) {
            final View sibling = ((ViewGroup) getParent()).getChildAt(i);

            if(sibling instanceof  MathViewBrace) {
                if (((MathViewBrace) sibling).mOrientation == Orientation.RIGHT)
                    nestedBracesDepth++;

                if (((MathViewBrace) sibling).mOrientation == Orientation.LEFT && nestedBracesDepth > 0)
                    nestedBracesDepth--;

                else if (((MathViewBrace) sibling).mOrientation == Orientation.LEFT) {
                    matchingBrace = sibling;
                    break;
                }
            }
        }

        if(matchingBrace != null)
            height = matchingBrace.getMeasuredHeight() - matchingBrace.getPaddingTop() - matchingBrace.getPaddingBottom();
        else
            height = calculateTextHeight();

        mAlignmentBaseline = (matchingBrace != null) ? getAlignmentBaseline(matchingBrace) : height / 2 + getPaddingTop();

        return height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        applyPaddingAndMargin();

        int height;

        if(mOrientation == Orientation.LEFT)
            height = calculateLeftBraceHeight();
        else if(mOrientation == Orientation.RIGHT)
            height = calculateRightBraceHeight();
        else
            throw new IllegalStateException("orientation must be Orientation.LEFT or Orientation.RIGHT");

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + (int) (mTextSize * BRACE_WIDTH) + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + height + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);

        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        Rect oldClipBounds = canvas.getClipBounds();

        canvas.clipRect(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(),
                getHeight() - getPaddingBottom(), Region.Op.REPLACE);

        if(mOrientation == Orientation.LEFT) {
            //draw left brace
            canvas.drawArc(
                    new RectF(
                            getPaddingLeft() + mStrokeWidth,
                            -(getHeight() - getPaddingTop() - getPaddingBottom()),
                            getPaddingLeft() + (getWidth() - getPaddingRight() - getPaddingLeft())*20,
                            getHeight() + (getHeight() - getPaddingTop() - getPaddingBottom())),
                    90, 180,
                    false, mDrawingPaint);
        }
        else if(mOrientation == Orientation.RIGHT) {
            //draw right brace
            canvas.drawArc(
                    new RectF(
                            getWidth() - getPaddingRight() - (getWidth() - getPaddingRight() - getPaddingLeft())*20,
                            -(getHeight() - getPaddingTop() - getPaddingBottom()),
                            getWidth() - mStrokeWidth - getPaddingRight(),
                            getHeight() + (getHeight() - getPaddingTop() - getPaddingBottom())),
                    -270, -180,
                    false, mDrawingPaint);
        }
        else
            throw new IllegalStateException("orientation must be Orientation.LEFT or Orientation.RIGHT");

        canvas.clipRect(oldClipBounds);
    }

    public void setOrientation(Orientation orientation)
    {
        if(orientation != mOrientation) {
            mOrientation = orientation;
            invalidate();
        }
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        UIUtils.setMargins(this, 0, 0, (int) (mTextSize * TOKEN_MARGIN_FACTOR), 0);

        //reduce padding so the braces don't take so much space
        setPadding(
                getPaddingLeft() * 3 / 4,
                getPaddingTop(),
                getPaddingRight() * 3 / 4,
                getPaddingBottom());
    }

    public Orientation getOrientation() {
        return mOrientation;
    }

    @Override
    public String getText()
    {
        return (mOrientation == Orientation.LEFT) ? "(" : ")";
    }
}
