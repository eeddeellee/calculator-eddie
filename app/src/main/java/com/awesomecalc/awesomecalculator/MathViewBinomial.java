package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.support.annotation.NonNull;

/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewBinomial extends CompositeMathView {
    protected MathView nView;
    protected MathView kView;
    protected static float NK_PADDING = 1/10f;
    protected static float BRACE_PADDING = 1/5f;

    public MathViewBinomial(Context ctx, @NonNull String text)
    {
        super(ctx, text);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if(getChildCount() < 2)
            throw new IllegalStateException("view not initialized properly");

        nView = (MathView)getChildAt(0);
        kView = (MathView)getChildAt(1);

        applyPaddingAndMargin();

        for (int i = 0; i < getChildCount(); i++)
        {
            MathView child = (MathView)getChildAt(i);

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));

            child.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));
        }

        int heightAboveCenter = nView.getMeasuredHeight() + (int)(mTextSize * NK_PADDING / 2f);
        int heightBelowCenter = kView.getMeasuredHeight() + (int)(mTextSize * NK_PADDING / 2f);

        mAlignmentBaseline = heightAboveCenter + getPaddingTop();

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + (int) (mTextSize * BRACE_PADDING * 2)
                        + Math.max(nView.getMeasuredWidth(), kView.getMeasuredWidth()) + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom(), heightMeasureSpec));

    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();
        UIUtils.addPadding(this,
                (int) (mTextSize * 0.1f),
                (int) (mTextSize * 0.1f),
                (int) (mTextSize * 0.1f),
                (int) (mTextSize * 0.1f));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        int nViewLeft;
        int kViewLeft;

        if(nView.getMeasuredWidth() >= kView.getMeasuredWidth()) {
            nViewLeft = getPaddingLeft() + (int)(mTextSize * BRACE_PADDING);
            kViewLeft = getPaddingLeft() + (int)(mTextSize * BRACE_PADDING)
                    + (int)((nView.getMeasuredWidth() - kView.getMeasuredWidth()) / 2f);
        }
        else {
            kViewLeft = getPaddingLeft() + (int)(mTextSize * BRACE_PADDING);
            nViewLeft = getPaddingLeft() + (int)(mTextSize * BRACE_PADDING)
                    + (int)((kView.getMeasuredWidth() - nView.getMeasuredWidth()) / 2f);
        }

        nView.layout(
                nViewLeft,
                getPaddingTop(),
                nViewLeft + nView.getMeasuredWidth(),
                getPaddingTop() + nView.getMeasuredHeight());

        kView.layout(
                kViewLeft,
                nView.getBottom() + (int)(mTextSize * NK_PADDING),
                kViewLeft + kView.getMeasuredWidth(),
                nView.getBottom() + (int)(mTextSize * NK_PADDING) + kView.getMeasuredHeight());
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas) {
        super.dispatchDraw(canvas);
        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        Rect oldClipBounds = canvas.getClipBounds();

        Rect leftClipBounds = new Rect(
                getPaddingLeft(),
                getPaddingTop(),
                getPaddingLeft() + (int)(mTextSize * BRACE_PADDING),
                getHeight() - getPaddingBottom());

        Rect rightClipBounds = new Rect(
                getWidth() - getPaddingRight() - (int) (mTextSize * BRACE_PADDING),
                getPaddingTop(),
                getWidth() - getPaddingRight(),
                getHeight() - getPaddingBottom());

        canvas.clipRect(leftClipBounds, Region.Op.REPLACE);

        //draw left brace
        canvas.drawArc(
                new RectF(
                        leftClipBounds.left + mStrokeWidth,
                        -leftClipBounds.height() + leftClipBounds.top,
                        leftClipBounds.right + leftClipBounds.width() * 20,
                        leftClipBounds.height() * 2 + leftClipBounds.top),
                90, 180,
                false, mDrawingPaint);

        canvas.clipRect(rightClipBounds, Region.Op.REPLACE);

        //draw right brace
        canvas.drawArc(
                new RectF(
                        rightClipBounds.right - rightClipBounds.width()*20,
                        -rightClipBounds.height() + rightClipBounds.top,
                        rightClipBounds.right - mStrokeWidth,
                        rightClipBounds.height() * 2 + rightClipBounds.top),
                -270, -180,
                false, mDrawingPaint);

        canvas.clipRect(oldClipBounds, Region.Op.REPLACE);
    }

    @Override
    public String getText()
    {
        return "#binom(" + nView.getText() + "," + kView.getText() + ")";
    }
}
