package com.awesomecalc.awesomecalculator;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by Thilo on 12.07.2015.
 */
public class MathViewIntegral extends CompositeMathView {

    protected ImageView mIntegralView;
    protected Drawable mIntegralImage;
    protected Drawable mIntegralImageFocused;
    protected TextView mDxLabel;
    protected MathView mUpperBounds;
    protected MathView mLowerBounds;
    protected MathView mIntegratedFunc;
    protected static float SYMBOL_BOUNDS_PADDING = 0.0f;
    protected static float BOUNDS_FUNC_PADDING = 0.0f;
    protected static float FUNC_DX_PADDING = 0.0f;
    protected static float BOUNDS_BOUNDS_PADDING = 3/10f;
    protected static float DX_RIGHT_PADDING = 1/10f;
    protected static float SYMBOL_PADDING = 1/8f;

    public MathViewIntegral(Context ctx, @NonNull String text)
    {
        super(ctx, text);

        mIntegralView = new ImageView(getContext());

        mIntegralImage = UIUtils.createTintedSvgDrawable(getResources(), R.raw.integral_sign, mColor);
        mIntegralImageFocused = UIUtils.createTintedSvgDrawable(getResources(), R.raw.integral_sign, mFocusColor);

        mIntegralView.setImageDrawable(mIntegralImage);
        mIntegralView.setLayerType(View.LAYER_TYPE_SOFTWARE, null); //without this the svg-image will not show
        mIntegralView.setAdjustViewBounds(true);
        addView(mIntegralView, 0, new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        mDxLabel = new TextView(getContext());
        mDxLabel.setText("dx");
        addView(mDxLabel, new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
    }

    @Override
    public void setFocusColor(int color)
    {
        super.setFocusColor(color);

        // color of the integral symbol has to be changed
        mIntegralImageFocused = UIUtils.createTintedSvgDrawable(getResources(), R.raw.integral_sign, mFocusColor);
    }

    @Override
    public void setColor(int color)
    {
        super.setColor(color);

        // color of the integral symbol has to be changed
        mIntegralImage = UIUtils.createTintedSvgDrawable(getResources(), R.raw.integral_sign, mColor);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        mUpperBounds = (MathView)getChildAt(1);
        mLowerBounds = (MathView)getChildAt(2);
        mIntegratedFunc = (MathView)getChildAt(3);

        applyPaddingAndMargin();

        mUpperBounds.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));
        mLowerBounds.setTextSize(Math.max(mTextSize * SUPERSCRIPT_TEXTSCALE_FACTOR, mMinTextSize));
        mDxLabel.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTextSize);

        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);

            if(child == mIntegralView)
                continue;

            child.measure(
                    getChildMeasureSpec(widthMeasureSpec,
                            getPaddingLeft() + getPaddingRight(),
                            child.getLayoutParams().width),
                    getChildMeasureSpec(heightMeasureSpec,
                            getPaddingTop() + getPaddingBottom(),
                            child.getLayoutParams().height));
        }

        int heightAboveCenter = Math.max(mUpperBounds.getMeasuredHeight(), getAlignmentBaseline(mIntegratedFunc))
                + (int)(mTextSize * BOUNDS_BOUNDS_PADDING / 2f);

        int heightBelowCenter = Math.max(mLowerBounds.getMeasuredHeight(), mIntegratedFunc.getMeasuredHeight() - getAlignmentBaseline(mIntegratedFunc))
                + (int)(mTextSize * BOUNDS_BOUNDS_PADDING / 2f);

        mAlignmentBaseline = heightAboveCenter + getPaddingTop();

        mIntegralView.measure(
                MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
                MeasureSpec.makeMeasureSpec(heightAboveCenter + heightBelowCenter, MeasureSpec.EXACTLY));

        int combinedWidth = mIntegralView.getMeasuredWidth()
                + Math.max(mUpperBounds.getMeasuredWidth(), mLowerBounds.getMeasuredWidth() - mIntegralView.getMeasuredWidth()/3)
                + mIntegratedFunc.getMeasuredWidth() + mDxLabel.getMeasuredWidth();

        setMeasuredDimension(
                resolveSize(getPaddingLeft() + combinedWidth + getPaddingRight(), widthMeasureSpec),
                resolveSize(getPaddingTop() + heightAboveCenter + heightBelowCenter + getPaddingBottom(), heightMeasureSpec));
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b)
    {
        mIntegralView.layout(
                getPaddingLeft(),
                getPaddingTop(),
                getPaddingLeft() + mIntegralView.getMeasuredWidth(),
                getPaddingTop() + mIntegralView.getMeasuredHeight());

        mUpperBounds.layout(
                mIntegralView.getRight()+ (int)(mTextSize * SYMBOL_BOUNDS_PADDING),
                getPaddingTop(),
                mIntegralView.getRight() + (int)(mTextSize * SYMBOL_BOUNDS_PADDING) + mUpperBounds.getMeasuredWidth(),
                getPaddingTop() + mUpperBounds.getMeasuredHeight());

        mLowerBounds.layout(
                mIntegralView.getRight() + (int)(mTextSize * SYMBOL_BOUNDS_PADDING) - mIntegralView.getMeasuredWidth()/3,
                mUpperBounds.getBottom() + (int)(mTextSize * BOUNDS_BOUNDS_PADDING),
                mIntegralView.getRight() + (int)(mTextSize * SYMBOL_BOUNDS_PADDING) + mLowerBounds.getMeasuredWidth() - mIntegralView.getMeasuredWidth()/3,
                mUpperBounds.getBottom() + (int)(mTextSize * BOUNDS_BOUNDS_PADDING) + mLowerBounds.getMeasuredHeight());

        int integratedFuncLeft = Math.max(mLowerBounds.getRight(), mUpperBounds.getRight()) + (int)(mTextSize * BOUNDS_FUNC_PADDING);
        int integratedFuncTop = mAlignmentBaseline - mIntegratedFunc.getMeasuredHeight()/2;

        mIntegratedFunc.layout(
                integratedFuncLeft,
                integratedFuncTop,
                integratedFuncLeft + mIntegratedFunc.getMeasuredWidth(),
                integratedFuncTop + mIntegratedFunc.getMeasuredHeight());

        int dxLeft = mIntegratedFunc.getRight() + (int)(mTextSize * FUNC_DX_PADDING);
        int dxTop = mAlignmentBaseline - mDxLabel.getMeasuredHeight()/2;

        mDxLabel.layout(
                dxLeft,
                dxTop,
                dxLeft + mDxLabel.getMeasuredWidth(),
                dxTop + mDxLabel.getMeasuredHeight());
    }

    @Override
    protected void dispatchDraw(@NonNull Canvas canvas)
    {
        super.dispatchDraw(canvas);
        mDrawingPaint.setStyle(Paint.Style.STROKE);
        mDrawingPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        mDxLabel.setTextColor(mDrawingPaint.getColor());

       if(mHasCaret)
            mIntegralView.setImageDrawable(mIntegralImageFocused);
        else
            mIntegralView.setImageDrawable(mIntegralImage);
    }

    @Override
    protected void applyPaddingAndMargin()
    {
        super.applyPaddingAndMargin();

        mDxLabel.setPadding(0, 0, (int) (mTextSize * DX_RIGHT_PADDING), 0);

        mIntegralView.setPadding(
                (int) (mTextSize * SYMBOL_PADDING),
                (int) (mTextSize * SYMBOL_PADDING),
                (int) (mTextSize * SYMBOL_PADDING),
                (int) (mTextSize * SYMBOL_PADDING));
    }

    @Override
    public MathView findCaret()
    {
        MathView v = null;

        if(mHasCaret)
            return this;

        for (int i = 0; i < getChildCount() ; i++)
        {
            if(!(getChildAt(i) instanceof MathView))
                continue;

            v = ((MathView) getChildAt(i)).findCaret();

            if(v != null)
                return v;
        }

        return v;
    }

    @Override
    public String getText()
    {
        return "#integral(" + mUpperBounds.getText() + "," + mLowerBounds.getText() + "," + mIntegratedFunc.getText() + ")";
    }
}
