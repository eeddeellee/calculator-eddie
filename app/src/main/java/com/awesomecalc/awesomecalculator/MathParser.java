package com.awesomecalc.awesomecalculator;

import java.util.ArrayList;

/**
 * Created by Thilo on 15.06.2015.
 */
public class MathParser extends Object {

    public static class ParseException extends RuntimeException {
        public ParseException(String e)
        {
            super(e);
        }
    }

    public static boolean isOperator(Character c)
    {
        return  c == '+' ||
                c == '-' ||
                c == '/' ||
                c == '%' ||
                c == '*' ||
                c == '!';
    }

    public static boolean hasSubTokens(String inputExpr)
    {
        if(inputExpr == null)
            throw new IllegalArgumentException("input can not be null");

        inputExpr = removeSpaces(inputExpr);

        int nestedBraceDepth = 0;

        if(inputExpr.length() == 0 || inputExpr.charAt(0) != '#')
            return false;

        for (int i = inputExpr.indexOf('('); i < inputExpr.length(); i++) {
            if(i == -1) //'(' was not found
                return false;

            if(inputExpr.charAt(i) == '(')
                nestedBraceDepth++;

            if(inputExpr.charAt(i) == ')')
                nestedBraceDepth--;

            if(nestedBraceDepth <= 0 && i != inputExpr.length()-1)
                return false;
        }

        return true;
    }

    public static boolean isTokenChain(String inputExpr)
    {
        if(inputExpr == null)
            throw new IllegalArgumentException("input can not be null");

        inputExpr = removeSpaces(inputExpr);

        if(hasSubTokens(inputExpr) || inputExpr.length() <= 1)
            return false;

        for (int i = 0; i < inputExpr.length(); i++) {
            if(isOperator(inputExpr.charAt(i)) || inputExpr.charAt(i) == ',')
                return true;
        }

        return false;
    }

    /**
     * @param originStr the string to look in
     * @param subStr the string to look for
     * @param index index to look at for the substring
     * @return true if originStr has the specified substring at index
     */
    public static boolean hasSubstringAt(String originStr, String subStr, int index)
    {
        if(originStr.length() < subStr.length() + index)
            return false;

        return subStr.equals(originStr.substring(index, index+subStr.length()));
    }

    private static String removeSpaces(String str)
    {
        return str.replaceAll("\\s+", "");
    }

    /**
     * @param inputExpr mathematical expression to be tokenized
     * @return returns single token if expression can not be tokenized any further. Tokens can itself contain other tokens
     */
    public static ArrayList<String> tokenizeForView(String inputExpr)
    {
        if(inputExpr == null)
            throw new IllegalArgumentException("input can not be null");

        inputExpr = removeSpaces(inputExpr);

        if(MathParser.hasSubTokens(inputExpr)) {
            return MathParser.tokenizeForView(inputExpr.substring(inputExpr.indexOf('(') + 1, inputExpr.length() - 1)); //isolate subtokens
        }
        else if(!MathParser.isTokenChain(inputExpr))
            //no subtokens and not a token chain -> not further tokenizeable -> return empty list
            return new ArrayList<>();

        ArrayList<String> tokenList = new ArrayList<String>();
        int bracketDepth = 0;
        String temp = "";

        //parse ',' delimited tokens
        for (int i = 0; i < inputExpr.length(); i++)
        {
            if(inputExpr.charAt(i) == '(') {
                bracketDepth++;
                temp += '(';
            }
            else if(inputExpr.charAt(i) == ')') {
                bracketDepth--;
                temp += ')';
            }
            else if(inputExpr.charAt(i) == ',' && bracketDepth == 0) {
                tokenList.add(temp);
                temp = "";
            }
            else {
                temp += inputExpr.charAt(i);
            }
        }

        if(bracketDepth != 0 && inputExpr.length() > 1)
            throw new ParseException("can not tokenize: brackets don't add up");

        if(!tokenList.isEmpty()) {
            tokenList.add(temp);
            return tokenList;
        }
        else {
            temp = "";
        }

        //parse normal tokens
        for (int i = 0; i < inputExpr.length();) {
            if(isOperator(inputExpr.charAt(i))) /* parse operators */ {
                tokenList.add(Character.toString(inputExpr.charAt(i)));
                i++;
            }
            else if (inputExpr.charAt(i) == '#') /* parse functions */ {
                temp += '#';
                i++;

                while(i < inputExpr.length()
                        && (bracketDepth > 0 || Character.isLetter(inputExpr.charAt(i)) ||  inputExpr.charAt(i) == '('))
                {
                    temp += inputExpr.charAt(i);

                    if(inputExpr.charAt(i) == '(')
                        bracketDepth++;

                    else if(inputExpr.charAt(i) == ')')
                        bracketDepth--;

                    i++;
                }

                tokenList.add(temp);
                temp = "";
            }
            else if(inputExpr.charAt(i) == '(' || inputExpr.charAt(i) == ')') {
                tokenList.add((Character.toString(inputExpr.charAt(i))));
                i++;
            }
            else /*parse numbers and variables */ {
                while(i < inputExpr.length()
                        && !(inputExpr.charAt(i) == '(' || inputExpr.charAt(i) == ')' || inputExpr.charAt(i) == '#' || isOperator(inputExpr.charAt(i)) ))
                {
                    temp += inputExpr.charAt(i);
                    i++;
                }

                tokenList.add(temp);
                temp = "";
            }
        }

        return tokenList;
    }

    public static ArrayList<String> tokenizeForEval(String mathExpr)
    {
        ArrayList<String> output = new ArrayList<String>();
        int bracketDepth = 0;
        String temp = "";

        mathExpr = removeSpaces(mathExpr);

        //parse ',' delimited tokens
        for (int i = 0; i < mathExpr.length(); i++)
        {
            if(mathExpr.charAt(i) == '(') {
                bracketDepth++;
                temp += '(';
            }
            else if(mathExpr.charAt(i) == ')') {
                bracketDepth--;
                temp += ')';
            }
            else if(mathExpr.charAt(i) == ',' && bracketDepth == 0) {
                output.add(temp);
                temp = "";
            }
            else {
                temp += mathExpr.charAt(i);
            }
        }

        if(bracketDepth != 0)
            throw new ParseException("can not tokenize: brackets don't add up");

        if(!output.isEmpty()) {
            output.add(temp);
            return output;
        }
        else {
            temp = "";
        }

        //parse normal tokens
        for (int i = 0; i < mathExpr.length();)
        {
            if(isOperator(mathExpr.charAt(i))) //parse operators
            {
                output.add(Character.toString(mathExpr.charAt(i)));
                i++;
            }
            else if (mathExpr.charAt(i) == '#') //parse functions
            {
                temp += '#';
                i++;

                while(i < mathExpr.length()
                        && (bracketDepth > 0 || Character.isLetter(mathExpr.charAt(i)) ||  mathExpr.charAt(i) == '('))
                {
                    temp += mathExpr.charAt(i);

                    if(mathExpr.charAt(i) == '(')
                        bracketDepth++;

                    else if(mathExpr.charAt(i) == ')')
                        bracketDepth--;

                    i++;
                }

                output.add(temp);
                temp = "";
            }
            else if(mathExpr.charAt(i) == '(') //parse bracketed subtokens
            {
                while(i < mathExpr.length()
                        && (bracketDepth > 0 || mathExpr.charAt(i) == '('))
                {
                    temp += mathExpr.charAt(i);

                    if(mathExpr.charAt(i) == '(')
                        bracketDepth++;

                    else if(mathExpr.charAt(i) == ')')
                        bracketDepth--;

                    i++;
                }

                output.add(temp);
                temp = "";
            }
            else //parse numbers and variables
            {
                while(i < mathExpr.length()
                        && !(mathExpr.charAt(i) == '(' || mathExpr.charAt(i) == '#' || isOperator(mathExpr.charAt(i)) ))
                {
                    temp += mathExpr.charAt(i);
                    i++;
                }

                output.add(temp);
                temp = "";
            }
        }

        return output;
    }

    /**
     * gets the function name of a token that is guaranteed to be a function
     * this function will NOT check if the token is valid!!!
     *
     * @param token token of a function of the format #foo(arg1,arg2...)
     * @return function name
     */
    public static String functionNameFromToken(String token)
    {
        if(token == null)
            throw new IllegalArgumentException("input can not be null");

        String buf = "";

        token = removeSpaces(token);

        if(token.charAt(0) != '#')
            throw new IllegalArgumentException("wrong token format: not a valid function");

        //start at 1 to ignore the leading '#'
        for(int i=1; token.charAt(i) != '('; i++) {
            if(token.charAt(i) == '#'
                    || token.charAt(i) == ')'
                    || token.charAt(i) == '@'
                    || token.charAt(i) == '$'
                    || token.charAt(i) == '~'
                    || token.charAt(i) == ','
                    || token.charAt(i) == '.'
                    || token.charAt(i) == '?'
                    || token.charAt(i) == '{'
                    || token.charAt(i) == '}'
                    || token.charAt(i) == '&'
                    || token.charAt(i) == '§'
                    || token.charAt(i) == '"'
                    || token.charAt(i) == '!'
                    || token.charAt(i) == '°'
                    || token.charAt(i) == '^'
                    || token.charAt(i) == '+'
                    || token.charAt(i) == '-'
                    || token.charAt(i) == '='
                    || token.charAt(i) == '/'
                    || token.charAt(i) == '\\'
                    || token.charAt(i) == '*'
                    || token.charAt(i) == '|'
                    || token.charAt(i) == '<'
                    || token.charAt(i) == '>')
                throw new IllegalArgumentException("wrong token format: not a valid function");
        }

        for(int i=1; token.charAt(i) != '('; i++)
            buf += token.charAt(i);

        return buf;
    }
}
