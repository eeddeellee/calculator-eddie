package com.awesomecalc.awesomecalculator;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by Thilo on 10/30/2015.
 */
@RunWith(AndroidJUnit4.class)
public class MathParserTest {

    @Test(timeout = 100)
    public void test_hasSubTokens()
    {
        assertThat(MathParser.hasSubTokens("#log(1,2)"), is(true));

        assertThat(MathParser.hasSubTokens(" #log(1+2, 3+#log(4,5) ) "), is(true));

        assertThat(MathParser.hasSubTokens("1+2"), is(false));

        assertThat(MathParser.hasSubTokens("1 + #log(1, 2)"), is(false));

        assertThat(MathParser.hasSubTokens("#log(3,4) + #log(1,2)"), is(false));

        try {
            MathParser.hasSubTokens(null);
            fail("expected exception wasn't thrown");
        }
        catch(IllegalArgumentException e) {}

    }

    @Test(timeout = 100)
    public void test_isTokenChain()
    {
        assertThat(MathParser.isTokenChain("#log(3,4) + #log(1,2)"), is(true));

        assertThat(MathParser.isTokenChain("#log(3,#log(1,2)) + 2"), is(true));

        assertThat(MathParser.isTokenChain("1+2"), is(true));

        assertThat(MathParser.isTokenChain("#log(3,#log(1,2))"), is(false));

        assertThat(MathParser.isTokenChain("#log(3,4)"), is(false));

        assertThat(MathParser.isTokenChain("#log(3,4/5+6)"), is(false));

        try {
            MathParser.isTokenChain(null);
            fail("expected exception wasn't thrown");
        }
        catch(IllegalArgumentException e) {}
    }

    @Test(timeout = 100)
    public void test_functionNameFromToken()
    {
        assertThat(MathParser.functionNameFromToken("#foo()"), is("foo"));

        assertThat(MathParser.functionNameFromToken(" #foo(3, 4) "), is("foo"));

        assertThat(MathParser.functionNameFromToken(" #foo(3, #sin()) "), is("foo"));

        try {
            MathParser.functionNameFromToken("##fo)@$~,.?{}&§\"!^+-=/\\*|<>o(3, 4)");
            fail("expected exception wasn't thrown");
        }
        catch(IllegalArgumentException e) {}

        try {
            MathParser.functionNameFromToken(null);
            fail("expected exception wasn't thrown");
        }
        catch(IllegalArgumentException e) {}
    }

    @Test()
    public void test_tokenizeForView()
    {
        try {
            MathParser.tokenizeForView(null);
            fail("expected exception wasn't thrown");
        }
        catch(IllegalArgumentException e) {}

        assertThat(MathParser.tokenizeForView("")
                             .size(), is(0));

        ArrayList<String> testList = null;
        ArrayList<String> tokenList = null;

        tokenList = MathParser.tokenizeForView("#pow(1,2)");
        testList = new ArrayList<>(Arrays.asList(new String[]
                {
                    "1", "2"
                }));
        assertTrue(tokenList.equals(testList));

        tokenList = MathParser.tokenizeForView(" 1 +( 2) ");
        testList = new ArrayList<>(Arrays.asList(new String[]
                {
                        "1", "+", "(", "2", ")"
                }));
        assertTrue(tokenList.equals(testList));

        tokenList = MathParser.tokenizeForView(" #pow(1, 2) + 2 /3 ");
        testList = new ArrayList<>(Arrays.asList(new String[]
                {
                        "#pow(1,2)", "+", "2", "/", "3"
                }));
        assertTrue(tokenList.equals(testList));

        tokenList = MathParser.tokenizeForView("#pow(1,#pow(3,4))");
        testList = new ArrayList<>(Arrays.asList(new String[]
                {
                        "1", "#pow(3,4)"
                }));
        assertTrue(tokenList.equals(testList));

        tokenList = MathParser.tokenizeForView("1+a+(4!/cde)");
        testList = new ArrayList<>(Arrays.asList(new String[]
                {
                        "1", "+", "a", "+", "(", "4", "!", "/", "cde", ")"
                }));
        assertTrue(tokenList.equals(testList));
    }
}
